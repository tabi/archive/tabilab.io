---
title: About Tabi
subtitle: why you'd want to hang out with us
date: 2017-11-20
comments: false
---

This project's goal is to collaboratively develop an app for data collection, open source under the [MIT license](https://tldrlegal.com/license/mit-license). This means that everybody can use the app and it's backend, including you and your institution. It targets the needs of (national) statistical institutes.

### stop reinventing the wheel

Statistical institutes across the globe face similar problems. We all have to adapt to new technologies. Most of us struggle with declining responses. Let's bring data collection into the 21<sup>st</sup> century and get ahead of the curve. By walking this path together, we can prevent reinventing the wheel dozens of times.

Instead of baking many small muffins, let's pool our efforts and bake a big cake together. Everybody can eat it! You don't like strawberries? Fret not, we develop the base together and you can tailor it to your needs.

### the vision

Using an app, we can measure instead of ask. This has tremendous benefits.

- Ease the strain on respondents. The more we measure, the less we have to ask.
- Get *independent* data with *different* noise and bias.
- Create added value by *giving back* information.

But wait! There's more. Because the burden on respondents is lower, we are not bound by questionnaire topics anymore. Collect data for studies on relocation, time use, budget spending, health and more in one go.

As a start, we focus on relocation measurement via GPS and other sensors. This, however, is just the low hanging fruit. Over time, the functionality will grow. Maybe it's your contributions that will make this happen.

### the source

The source code can be found in a [GitLab repository](https://gitlab.com/tabi). Sub-projects include the [app](https://gitlab.com/tabi-app) itself, the [backend](https://gitlab.com/tabi-backend) to collect measured data and a [documentation](https://gitlab.com/tabi-documentation) folder. [This website](https://gitlab.com/tabi.gitlab.io) can be found there as well, for the sake of transparency.

![Tabi Project Overview](/images/tabi-overview.svg)

Feel free to fork it and build your own. We greatly appreciate any code contributions, patches, bugfixes or comments. Together we can create something great.

---

### contact

Feel free to email us at <WINHelpdesk@cbs.nl>.
